Shader "Unlit/ShaderMasktest"
{
    Properties
    {
        _FogColor("Color", Color) = (0, 0, 0, 1)
        _MaskPos1("MaskPos1", Vector) = (0, 0, 0, 0)
        _MaskSize1("MaskSize1", Float) = 1
        _testOffset("TestOffset", Vector) = (0, 0, 0, 0)
        [HideInInspector][NoScaleOffset]unity_Lightmaps("unity_Lightmaps", 2DArray) = "" {}
        [HideInInspector][NoScaleOffset]unity_LightmapsInd("unity_LightmapsInd", 2DArray) = "" {}
        [HideInInspector][NoScaleOffset]unity_ShadowMasks("unity_ShadowMasks", 2DArray) = "" {}
    }
    SubShader
    {
        Tags
        {
            "RenderPipeline"="UniversalPipeline"
            "RenderType"="Transparent"
            "UniversalMaterialType" = "Unlit"
            "Queue"="Transparent"
        }

        Stencil
        {
            Ref 1
            Comp Always
            Pass Replace
        }
        
        Pass
        {
            Name "Sprite Unlit"
            Tags
            {
                "LightMode" = "Universal2D"
            }

            // Render State
            Cull Off
        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
        ZTest LEqual
        ZWrite Off

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 2.0
        #pragma exclude_renderers d3d11_9x
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            // PassKeywords: <None>
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define ATTRIBUTES_NEED_COLOR
            #define VARYINGS_NEED_POSITION_WS
            #define VARYINGS_NEED_TEXCOORD0
            #define VARYINGS_NEED_COLOR
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_SPRITEUNLIT
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            float4 color : COLOR;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float3 positionWS;
            float4 texCoord0;
            float4 color;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float3 WorldSpacePosition;
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float3 interp0 : TEXCOORD0;
            float4 interp1 : TEXCOORD1;
            float4 interp2 : TEXCOORD2;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyz =  input.positionWS;
            output.interp1.xyzw =  input.texCoord0;
            output.interp2.xyzw =  input.color;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.positionWS = input.interp0.xyz;
            output.texCoord0 = input.interp1.xyzw;
            output.color = input.interp2.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _FogColor;
        float3 _MaskPos1;
        float _MaskSize1;
        float2 _testOffset;
        CBUFFER_END

        // Object and Global properties

            // Graph Functions
            
        void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
        {
            RGBA = float4(R, G, B, A);
            RGB = float3(R, G, B);
            RG = float2(R, G);
        }

        void Unity_PolarCoordinates_float(float2 UV, float2 Center, float RadialScale, float LengthScale, out float2 Out)
        {
            float2 delta = UV - Center;
            float radius = length(delta) * 2 * RadialScale;
            float angle = atan2(delta.x, delta.y) * 1.0/6.28 * LengthScale;
            Out = float2(radius, angle);
        }

        void Unity_Clamp_float(float In, float Min, float Max, out float Out)
        {
            Out = clamp(In, Min, Max);
        }

        void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
        {
            Out = A - B;
        }

        void Unity_Add_float2(float2 A, float2 B, out float2 Out)
        {
            Out = A + B;
        }

        void Unity_Divide_float2(float2 A, float2 B, out float2 Out)
        {
            Out = A / B;
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float3 BaseColor;
            float Alpha;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_a6913573cbf4434a8c8a7afa23a77f1b_Out_0 = _FogColor;
            float _Split_6468e41b5fa141cd8187ea84414520ad_R_1 = _Property_a6913573cbf4434a8c8a7afa23a77f1b_Out_0[0];
            float _Split_6468e41b5fa141cd8187ea84414520ad_G_2 = _Property_a6913573cbf4434a8c8a7afa23a77f1b_Out_0[1];
            float _Split_6468e41b5fa141cd8187ea84414520ad_B_3 = _Property_a6913573cbf4434a8c8a7afa23a77f1b_Out_0[2];
            float _Split_6468e41b5fa141cd8187ea84414520ad_A_4 = _Property_a6913573cbf4434a8c8a7afa23a77f1b_Out_0[3];
            float4 _Combine_4bfa826c0c8a45dba885d77257526e16_RGBA_4;
            float3 _Combine_4bfa826c0c8a45dba885d77257526e16_RGB_5;
            float2 _Combine_4bfa826c0c8a45dba885d77257526e16_RG_6;
            Unity_Combine_float(_Split_6468e41b5fa141cd8187ea84414520ad_R_1, _Split_6468e41b5fa141cd8187ea84414520ad_G_2, _Split_6468e41b5fa141cd8187ea84414520ad_B_3, 0, _Combine_4bfa826c0c8a45dba885d77257526e16_RGBA_4, _Combine_4bfa826c0c8a45dba885d77257526e16_RGB_5, _Combine_4bfa826c0c8a45dba885d77257526e16_RG_6);
            float2 _PolarCoordinates_99a2b16b97a34fe4adbb00a5c110894d_Out_4;
            Unity_PolarCoordinates_float(IN.uv0.xy, float2 (0.5, 0.5), 1, 1, _PolarCoordinates_99a2b16b97a34fe4adbb00a5c110894d_Out_4);
            float _Split_54b1d6e238cd4974ae6dfa6f075581f0_R_1 = _PolarCoordinates_99a2b16b97a34fe4adbb00a5c110894d_Out_4[0];
            float _Split_54b1d6e238cd4974ae6dfa6f075581f0_G_2 = _PolarCoordinates_99a2b16b97a34fe4adbb00a5c110894d_Out_4[1];
            float _Split_54b1d6e238cd4974ae6dfa6f075581f0_B_3 = 0;
            float _Split_54b1d6e238cd4974ae6dfa6f075581f0_A_4 = 0;
            float _Clamp_4d8a770454db4d069019074efa1ec2c7_Out_3;
            Unity_Clamp_float(_Split_54b1d6e238cd4974ae6dfa6f075581f0_R_1, 0, 1, _Clamp_4d8a770454db4d069019074efa1ec2c7_Out_3);
            float3 _Property_fc303347db5548d0882dca31b830ba6a_Out_0 = _MaskPos1;
            float3 _Subtract_bf6e8faf00f441c3908c2ecb93355e11_Out_2;
            Unity_Subtract_float3(IN.WorldSpacePosition, _Property_fc303347db5548d0882dca31b830ba6a_Out_0, _Subtract_bf6e8faf00f441c3908c2ecb93355e11_Out_2);
            float2 _Property_ecc5a5cc9f9b42e0b7f5b9cad461f7cc_Out_0 = _testOffset;
            float2 _Add_152519a75b1a492f8024293d127cbe93_Out_2;
            Unity_Add_float2((_Subtract_bf6e8faf00f441c3908c2ecb93355e11_Out_2.xy), _Property_ecc5a5cc9f9b42e0b7f5b9cad461f7cc_Out_0, _Add_152519a75b1a492f8024293d127cbe93_Out_2);
            float _Property_95331d2d0c6b44c4b2fbc3555b1b837d_Out_0 = _MaskSize1;
            float2 _Divide_d70f034eb6c644dda6bb433f547453c1_Out_2;
            Unity_Divide_float2(_Add_152519a75b1a492f8024293d127cbe93_Out_2, (_Property_95331d2d0c6b44c4b2fbc3555b1b837d_Out_0.xx), _Divide_d70f034eb6c644dda6bb433f547453c1_Out_2);
            float2 _PolarCoordinates_4a15fe7d44a946179dd8ccda295857e3_Out_4;
            Unity_PolarCoordinates_float(_Divide_d70f034eb6c644dda6bb433f547453c1_Out_2, float2 (0.5, 0.5), 1, 1, _PolarCoordinates_4a15fe7d44a946179dd8ccda295857e3_Out_4);
            float _Split_aaf1e013635d4622b9884cdd8a7decee_R_1 = _PolarCoordinates_4a15fe7d44a946179dd8ccda295857e3_Out_4[0];
            float _Split_aaf1e013635d4622b9884cdd8a7decee_G_2 = _PolarCoordinates_4a15fe7d44a946179dd8ccda295857e3_Out_4[1];
            float _Split_aaf1e013635d4622b9884cdd8a7decee_B_3 = 0;
            float _Split_aaf1e013635d4622b9884cdd8a7decee_A_4 = 0;
            float _Clamp_20551956225a453bad15447ca079aad4_Out_3;
            Unity_Clamp_float(_Split_aaf1e013635d4622b9884cdd8a7decee_R_1, 0, 1, _Clamp_20551956225a453bad15447ca079aad4_Out_3);
            float _Multiply_70f84eb4adab438092700ea6a05d301b_Out_2;
            Unity_Multiply_float(_Clamp_4d8a770454db4d069019074efa1ec2c7_Out_3, _Clamp_20551956225a453bad15447ca079aad4_Out_3, _Multiply_70f84eb4adab438092700ea6a05d301b_Out_2);
            float _Lerp_7b693b7d900641c9a15e4264772410fa_Out_3;
            Unity_Lerp_float(0, _Split_6468e41b5fa141cd8187ea84414520ad_A_4, _Multiply_70f84eb4adab438092700ea6a05d301b_Out_2, _Lerp_7b693b7d900641c9a15e4264772410fa_Out_3);
            surface.BaseColor = _Combine_4bfa826c0c8a45dba885d77257526e16_RGB_5;
            surface.Alpha = _Lerp_7b693b7d900641c9a15e4264772410fa_Out_3;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





            output.WorldSpacePosition =          input.positionWS;
            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/SpriteUnlitPass.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "Sprite Unlit"
            Tags
            {
                "LightMode" = "UniversalForward"
            }

            // Render State
            Cull Off
        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
        ZTest LEqual
        ZWrite Off

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 2.0
        #pragma exclude_renderers d3d11_9x
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            // PassKeywords: <None>
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define ATTRIBUTES_NEED_COLOR
            #define VARYINGS_NEED_POSITION_WS
            #define VARYINGS_NEED_TEXCOORD0
            #define VARYINGS_NEED_COLOR
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_SPRITEFORWARD
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            float4 color : COLOR;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float3 positionWS;
            float4 texCoord0;
            float4 color;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float3 WorldSpacePosition;
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float3 interp0 : TEXCOORD0;
            float4 interp1 : TEXCOORD1;
            float4 interp2 : TEXCOORD2;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyz =  input.positionWS;
            output.interp1.xyzw =  input.texCoord0;
            output.interp2.xyzw =  input.color;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.positionWS = input.interp0.xyz;
            output.texCoord0 = input.interp1.xyzw;
            output.color = input.interp2.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _FogColor;
        float3 _MaskPos1;
        float _MaskSize1;
        float2 _testOffset;
        CBUFFER_END

        // Object and Global properties

            // Graph Functions
            
        void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
        {
            RGBA = float4(R, G, B, A);
            RGB = float3(R, G, B);
            RG = float2(R, G);
        }

        void Unity_PolarCoordinates_float(float2 UV, float2 Center, float RadialScale, float LengthScale, out float2 Out)
        {
            float2 delta = UV - Center;
            float radius = length(delta) * 2 * RadialScale;
            float angle = atan2(delta.x, delta.y) * 1.0/6.28 * LengthScale;
            Out = float2(radius, angle);
        }

        void Unity_Clamp_float(float In, float Min, float Max, out float Out)
        {
            Out = clamp(In, Min, Max);
        }

        void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
        {
            Out = A - B;
        }

        void Unity_Add_float2(float2 A, float2 B, out float2 Out)
        {
            Out = A + B;
        }

        void Unity_Divide_float2(float2 A, float2 B, out float2 Out)
        {
            Out = A / B;
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float3 BaseColor;
            float Alpha;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_a6913573cbf4434a8c8a7afa23a77f1b_Out_0 = _FogColor;
            float _Split_6468e41b5fa141cd8187ea84414520ad_R_1 = _Property_a6913573cbf4434a8c8a7afa23a77f1b_Out_0[0];
            float _Split_6468e41b5fa141cd8187ea84414520ad_G_2 = _Property_a6913573cbf4434a8c8a7afa23a77f1b_Out_0[1];
            float _Split_6468e41b5fa141cd8187ea84414520ad_B_3 = _Property_a6913573cbf4434a8c8a7afa23a77f1b_Out_0[2];
            float _Split_6468e41b5fa141cd8187ea84414520ad_A_4 = _Property_a6913573cbf4434a8c8a7afa23a77f1b_Out_0[3];
            float4 _Combine_4bfa826c0c8a45dba885d77257526e16_RGBA_4;
            float3 _Combine_4bfa826c0c8a45dba885d77257526e16_RGB_5;
            float2 _Combine_4bfa826c0c8a45dba885d77257526e16_RG_6;
            Unity_Combine_float(_Split_6468e41b5fa141cd8187ea84414520ad_R_1, _Split_6468e41b5fa141cd8187ea84414520ad_G_2, _Split_6468e41b5fa141cd8187ea84414520ad_B_3, 0, _Combine_4bfa826c0c8a45dba885d77257526e16_RGBA_4, _Combine_4bfa826c0c8a45dba885d77257526e16_RGB_5, _Combine_4bfa826c0c8a45dba885d77257526e16_RG_6);
            float2 _PolarCoordinates_99a2b16b97a34fe4adbb00a5c110894d_Out_4;
            Unity_PolarCoordinates_float(IN.uv0.xy, float2 (0.5, 0.5), 1, 1, _PolarCoordinates_99a2b16b97a34fe4adbb00a5c110894d_Out_4);
            float _Split_54b1d6e238cd4974ae6dfa6f075581f0_R_1 = _PolarCoordinates_99a2b16b97a34fe4adbb00a5c110894d_Out_4[0];
            float _Split_54b1d6e238cd4974ae6dfa6f075581f0_G_2 = _PolarCoordinates_99a2b16b97a34fe4adbb00a5c110894d_Out_4[1];
            float _Split_54b1d6e238cd4974ae6dfa6f075581f0_B_3 = 0;
            float _Split_54b1d6e238cd4974ae6dfa6f075581f0_A_4 = 0;
            float _Clamp_4d8a770454db4d069019074efa1ec2c7_Out_3;
            Unity_Clamp_float(_Split_54b1d6e238cd4974ae6dfa6f075581f0_R_1, 0, 1, _Clamp_4d8a770454db4d069019074efa1ec2c7_Out_3);
            float3 _Property_fc303347db5548d0882dca31b830ba6a_Out_0 = _MaskPos1;
            float3 _Subtract_bf6e8faf00f441c3908c2ecb93355e11_Out_2;
            Unity_Subtract_float3(IN.WorldSpacePosition, _Property_fc303347db5548d0882dca31b830ba6a_Out_0, _Subtract_bf6e8faf00f441c3908c2ecb93355e11_Out_2);
            float2 _Property_ecc5a5cc9f9b42e0b7f5b9cad461f7cc_Out_0 = _testOffset;
            float2 _Add_152519a75b1a492f8024293d127cbe93_Out_2;
            Unity_Add_float2((_Subtract_bf6e8faf00f441c3908c2ecb93355e11_Out_2.xy), _Property_ecc5a5cc9f9b42e0b7f5b9cad461f7cc_Out_0, _Add_152519a75b1a492f8024293d127cbe93_Out_2);
            float _Property_95331d2d0c6b44c4b2fbc3555b1b837d_Out_0 = _MaskSize1;
            float2 _Divide_d70f034eb6c644dda6bb433f547453c1_Out_2;
            Unity_Divide_float2(_Add_152519a75b1a492f8024293d127cbe93_Out_2, (_Property_95331d2d0c6b44c4b2fbc3555b1b837d_Out_0.xx), _Divide_d70f034eb6c644dda6bb433f547453c1_Out_2);
            float2 _PolarCoordinates_4a15fe7d44a946179dd8ccda295857e3_Out_4;
            Unity_PolarCoordinates_float(_Divide_d70f034eb6c644dda6bb433f547453c1_Out_2, float2 (0, 0), 1, 1, _PolarCoordinates_4a15fe7d44a946179dd8ccda295857e3_Out_4);
            float _Split_aaf1e013635d4622b9884cdd8a7decee_R_1 = _PolarCoordinates_4a15fe7d44a946179dd8ccda295857e3_Out_4[0];
            float _Split_aaf1e013635d4622b9884cdd8a7decee_G_2 = _PolarCoordinates_4a15fe7d44a946179dd8ccda295857e3_Out_4[1];
            float _Split_aaf1e013635d4622b9884cdd8a7decee_B_3 = 0;
            float _Split_aaf1e013635d4622b9884cdd8a7decee_A_4 = 0;
            float _Clamp_20551956225a453bad15447ca079aad4_Out_3;
            Unity_Clamp_float(_Split_aaf1e013635d4622b9884cdd8a7decee_R_1, 0, 1, _Clamp_20551956225a453bad15447ca079aad4_Out_3);
            float _Multiply_70f84eb4adab438092700ea6a05d301b_Out_2;
            Unity_Multiply_float(_Clamp_4d8a770454db4d069019074efa1ec2c7_Out_3, _Clamp_20551956225a453bad15447ca079aad4_Out_3, _Multiply_70f84eb4adab438092700ea6a05d301b_Out_2);
            float _Lerp_7b693b7d900641c9a15e4264772410fa_Out_3;
            Unity_Lerp_float(0, _Split_6468e41b5fa141cd8187ea84414520ad_A_4, _Multiply_70f84eb4adab438092700ea6a05d301b_Out_2, _Lerp_7b693b7d900641c9a15e4264772410fa_Out_3);
            surface.BaseColor = _Combine_4bfa826c0c8a45dba885d77257526e16_RGB_5;
            surface.Alpha = _Lerp_7b693b7d900641c9a15e4264772410fa_Out_3;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





            output.WorldSpacePosition =          input.positionWS;
            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/SpriteUnlitPass.hlsl"

            ENDHLSL
        }
    }
    FallBack "Hidden/Shader Graph/FallbackError"
}