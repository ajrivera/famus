using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using UnityEngine;

public class Dud : MonoBehaviour
{
    public float speed;
    public float jumpforce;
    private bool locked = false;
    private float xmove = 0f;
    private float ymove = 0f;


    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!locked)
        {
            if (Input.GetKey(KeyCode.A))
                xmove = -1;
            else if (Input.GetKey(KeyCode.D))
                xmove = 1;
            else
                xmove = 0;
            //xmove = Input.GetAxisRaw("Horizontal");

            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector2(rb.velocity.x, jumpforce));
                print("w");
            }
        }
        ymove = rb.velocity.y;

    }
    void FixedUpdate()
    {
        if (!locked)
        {
            rb.velocity = new Vector2(speed * xmove, rb.velocity.y);
        }
    }
    private void controller()
    {
        if (Input.GetKeyDown(KeyCode.A)) {
            rb.velocity = new Vector2(-speed, rb.velocity.y);
            print("a");
        }
        else if(Input.GetKeyDown(KeyCode.D)) {
            rb.velocity = new Vector2(speed, rb.velocity.y);
            print("d");
        }
        if (Input.GetKeyDown(KeyCode.S)) { rb.velocity.Set(speed,rb.velocity.y)  ; }
    }
}
