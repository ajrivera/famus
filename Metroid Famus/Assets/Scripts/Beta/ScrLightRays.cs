using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

public class ScrLightRays : MonoBehaviour
{
    [SerializeField]
    private Collider2D[] shadowcastObjects;
    private EdgeCollider2D meshLimit;
    private Vector2 _rayPos;
    private LayerMask _mask;

    [SerializeField]
    private float angleOffset = 0.1f;
    [SerializeField]
    private float lightRadius = 10f;
    [SerializeField]
    private GameObject rays;
    private Transform _raysTransform;
    private Mesh _mesh;
    
    List<Vector3> finalVertices = new List<Vector3>();
    
    void Start()
    {
        _rayPos = transform.position;
        _mask = LayerMask.GetMask("Floor");
        
        meshLimit = GetComponent<EdgeCollider2D>();
        
        List<Vector2> meshPoints = new List<Vector2>();
        meshLimit.GetPoints(meshPoints);
        for (int i = 0; i < meshPoints.Count; i++)
        {
            meshPoints[i] *= lightRadius / 1.5f;
        }
        meshLimit.SetPoints(meshPoints);
        
        _raysTransform = rays.transform;
        _mesh = new Mesh {name = "rayMesh"};
        rays.GetComponent<MeshFilter>().mesh = _mesh;
        
    }

    void Update()
    {
        _rayPos = transform.position;
        _raysTransform.position = _rayPos;
        finalVertices.Clear();
        finalVertices.Capacity = 0;

        for (int i = 0; i < shadowcastObjects.Length; i++)
        {
            Vector2[] vertices = GetColliderVertices(shadowcastObjects[i]);

            for (int j = 0; j < vertices.Length; j++)
            {
                // Vector2 vertexPos = shadowcastObjects[i].transform.TransformPoint(vertices[j]);
                // Vector2 target = vertexPos - _rayPos;
                
                Vector2 target = vertices[j] - _rayPos;
                
                if (target.magnitude > lightRadius) continue;
                
                Vector2 direction1 = Quaternion.AngleAxis(angleOffset, Vector3.forward) * target;
                Vector2 direction2 = Quaternion.AngleAxis(-angleOffset, Vector3.forward) * target;
                
                RaycastHit2D hit1 = Physics2D.Raycast(_rayPos, direction1, lightRadius, _mask);
                RaycastHit2D hit2 = Physics2D.Raycast(_rayPos, direction2, lightRadius, _mask);
                
                finalVertices.Add(hit1.point);
                finalVertices.Add(hit2.point);
                
                if (hit1.collider == null)
                {
                    Debug.DrawLine(_rayPos, (direction1.normalized * lightRadius) + _rayPos, Color.green);
                }
                else
                {
                    Debug.DrawLine(_rayPos, hit1.point, Color.green);
                }

                if (hit2.collider == null)
                {
                    Debug.DrawLine(_rayPos, (direction2.normalized * lightRadius) + _rayPos, Color.red);
                }
                else
                {
                    Debug.DrawLine(_rayPos, hit2.point, Color.red);
                }
            }
        }

        Vector3[] interPoints = GetMainAreaIntersections(shadowcastObjects);
        foreach (Vector3 t in interPoints)
        {
            finalVertices.Add(t);
        }
        
        List<Vector3> orderedVertices = new List<Vector3>();
        orderedVertices.Clear();
        orderedVertices.Capacity = 0;
        
        foreach (Vector3 vector3 in finalVertices.OrderBy(c => Vector3.SignedAngle(_raysTransform.position, c - (Vector3)_rayPos, Vector3.forward)))
        {
            orderedVertices.Add(vector3);
        }

        orderedVertices.Add(_rayPos);     // Add center vertex.

        CreateMesh(orderedVertices.ToArray());
    }

    private void CreateMesh(Vector3[] points)
    {
        for (int i = 0; i < points.Length; i++)
        {
            points[i] -= new Vector3(_rayPos.x, _rayPos.y, 0);
        }
        
        _mesh.vertices = points;
        
        int[] tris = new int[points.Length * 3];

        int index = 0;
        
        for (int i = 0; i < points.Length - 1; i++)
        {
            tris[index] = points.Length - 1;
            tris[index + 1] = i;
            tris[index + 2] = i+1 < points.Length - 1 ? i+1 : 0;

            index += 3;
        }
        
        _mesh.triangles = tris;

        rays.GetComponent<MeshFilter>().mesh = _mesh;
    }
    
    private Vector2[] GetColliderVertices(Collider2D col)
    {
        Vector3[] points = col.CreateMesh(true, true).vertices;
        Vector2[] vertices = new Vector2[points.Length];
        
        for (int i = 0; i < points.Length; i++)
        {
            vertices[i] = new Vector2(points[i].x, points[i].y);
        }

        return vertices;
    }
    
    

    private Vector3[] GetMainAreaIntersections(Collider2D[] colliders)
    {
        List<Vector3> points = new List<Vector3>();
        points.Clear();
        points.Capacity = 0;
        
        

        foreach (Collider2D col in colliders)
        {
            if (!meshLimit.bounds.Intersects(col.bounds)) continue;

            
        }

        return points.ToArray();
    }
    
    
    // private void DrawCircle()
    // {
    //     float angle = 0f;
    //     Vector3[] points = new Vector3[segments + 1];
    //     
    //     for (int i = 0; i < segments; i++)
    //     {
    //         float x = Mathf.Sin(Mathf.Deg2Rad * angle) * lightRadius;
    //         float y = Mathf.Cos(Mathf.Deg2Rad * angle) * lightRadius;
    //
    //         points[i] = new Vector3(x, y,0f);
    //
    //         angle += (360f / segments);
    //     }
    //     points[points.Length - 1] = Vector3.zero;           // Add Center Vertex
    //
    //     CreateMesh(points);
    //     
    //     // Debugging
    //     for (int i = 0; i < _mesh.vertexCount; i++)
    //     {
    //         Debug.DrawLine(_mesh.vertices[i], Vector3.right *.1f);
    //     }
    //     
    // }
    
}