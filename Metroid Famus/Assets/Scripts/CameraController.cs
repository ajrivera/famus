using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    private Vector3 targetpos;
    public Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void FixedUpdate()
    {
        FollowTarget();
        
    }

    void FollowTarget()
    {
        targetpos = target.position;
        transform.position = Vector3.Lerp(transform.position, new Vector3(targetpos.x - offset.x,targetpos.y - offset.y,targetpos.z - offset.z), 0.05f);
    }
}
