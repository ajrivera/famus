using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalShaderController : MonoBehaviour
{
    private static readonly int CharPos = Shader.PropertyToID("_CharPos");
    void Start()
    {
        
    }
    
    void Update()
    {
        Shader.SetGlobalVector(CharPos, PlayerController.PlayerPosition);
    }
}
