using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public abstract class HealthControl : MonoBehaviour
{
    public enum ElementType
    {
        Physical,
        Acid,
        Fire,
        Thunder
    }
    [SerializeField,Range(0,500),BoxGroup("hp")]
    protected float hpmax;
    protected float hp;

    public bool iframe = false;
    public ElementType ET;
    
}
