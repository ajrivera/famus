using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class HealthControlPlayer : HealthControl
{

    // Start is called before the first frame update
    void Start()
    {
        hp = hpmax;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Enemy")
        {
            receiveDmg(10, ElementType.Physical);
            print("receive dmg");
        }
    }

    public void receiveDmg(float dmg, ElementType otherET)
    {
        if (!iframe)
        {
            if (otherET == ElementType.Physical)
            {
                hp -= dmg;
            }
            else
            {
                if (ET == otherET)
                    hp -= dmg / 1.5f;
                else
                    hp -= dmg * 1.5f;
            }
            if (hp <= 0)
            {
                hp = 0;
                dead();
            }
            print("player-"+dmg+" --- "+hp+"/"+hpmax);
            StartCoroutine(iframeCD());
        }
    }

    IEnumerator iframeCD()
    {
        iframe = true;
        yield return new WaitForSeconds(0.5f);
        iframe = false;
    }

// add more things to dead 
    private void dead()
    {

        Destroy(gameObject);
        
    }
    
}
