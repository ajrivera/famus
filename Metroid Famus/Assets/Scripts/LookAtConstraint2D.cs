using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Serialization;

[ExecuteAlways]
public class LookAtConstraint2D : MonoBehaviour
{
    [SerializeField]
    private Transform target;
    [SerializeField]
    private float angleOffset;
    [SerializeField, Range(0, 1)]
    private float smoothness;
    [SerializeField]
    private Transform dirTransform;
    [SerializeField, Range(0, 1)]
    private float weight = 1;

    [SerializeField]
    private bool spriteTile;
    [SerializeField, ShowIf("spriteTile")]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private bool raycastMode;
    [SerializeField, ShowIf("raycastMode")]
    private string[] collisionMask;
    private LayerMask layerMask;
    
    
    private RaycastHit2D hit;

    private Quaternion initRot;

    private void OnEnable()
    {
        initRot = transform.rotation;

        foreach (string t in collisionMask)
        {
            layerMask |= (1 << LayerMask.NameToLayer(t));
        }
    }

    void LateUpdate()
    {
        Vector2 lookat = new Vector2(target.position.x - transform.position.x,target.position.y - transform.position.y);

        float offsetAux = angleOffset;
        if (dirTransform) offsetAux *= dirTransform.localScale.x;

        Quaternion signedAngle =
            Quaternion.Euler(0f, 0f, Vector2.SignedAngle(transform.parent.rotation * Vector2.up, lookat));
        Quaternion newRot =
            Quaternion.Euler(0f, 0f, transform.parent.rotation.eulerAngles.z + signedAngle.eulerAngles.z + offsetAux);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Lerp(initRot, newRot, weight), smoothness);

        if (raycastMode)
        {
            hit = Physics2D.Raycast(transform.position, transform.right, 200, layerMask);
        }

        if (spriteTile)
        {
            if (raycastMode)
            {
                spriteRenderer.size = new Vector2((hit.point - (Vector2) transform.position).magnitude / transform.lossyScale.x, spriteRenderer.size.y);
            }
            else
            {
                spriteRenderer.size = new Vector2(((Vector2) target.position - (Vector2) transform.position).magnitude / transform.lossyScale.x, spriteRenderer.size.y);
            }
        }
        
    }
}
