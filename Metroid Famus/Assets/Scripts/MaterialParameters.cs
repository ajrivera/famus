using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

public class MaterialParameters : MonoBehaviour
{
    private MaterialPropertyBlock _parameters;
    private Renderer _rend;
    string[] propertyNames;

    [SerializeField] private List<TextureParam> texParam;
    [SerializeField] private List<IntParam> intParam;
    [SerializeField] private List<FloatParam> floatParam;
    [SerializeField] private List<VectorParam> vectorParam;
    [SerializeField] private List<ColorParam> colorParam;
    
    void Awake()
    {
    }

    private void OnEnable()
    {
        _parameters = new MaterialPropertyBlock();
        _rend = GetComponent<Renderer>();
        _rend.GetPropertyBlock(_parameters);
        
        texParam = new List<TextureParam>();
        intParam = new List<IntParam>();
        floatParam = new List<FloatParam>();
        vectorParam = new List<VectorParam>();
        colorParam = new List<ColorParam>();
    }

    [Button("Apply Properties")]
    void ApplyProperties()
    {
        _parameters = new MaterialPropertyBlock();
        _rend = GetComponent<Renderer>();
        _rend.GetPropertyBlock(_parameters);

        foreach (TextureParam texpar in texParam)
        {
            _parameters.SetTexture(texpar.paramName, texpar.texture);
        }
        foreach (FloatParam floatpar in floatParam)
        {
            _parameters.SetFloat(floatpar.paramName, floatpar.f);
        }
        foreach (VectorParam vecpar in vectorParam)
        {
            _parameters.SetVector(vecpar.paramName, vecpar.vec);
        }
        foreach (ColorParam colpar in colorParam)
        {
            _parameters.SetColor(colpar.paramName, colpar.col);
        }
        _rend.SetPropertyBlock(_parameters);
    }
    
    [Button("GetProperties")]
    void Properties()
    {
        _parameters = new MaterialPropertyBlock();
        
        texParam = new List<TextureParam>();
        intParam = new List<IntParam>();
        floatParam = new List<FloatParam>();
        vectorParam = new List<VectorParam>();
        colorParam = new List<ColorParam>();
        
        _rend = GetComponent<Renderer>();
        
        _rend.GetPropertyBlock(_parameters);

        Shader shad = _rend.sharedMaterial.shader;

        propertyNames = new string[shad.GetPropertyCount()];

        
        
        for (int i = 0; i < shad.GetPropertyCount(); i++)
        {
            propertyNames[i] = shad.GetPropertyName(i);
            int propId = shad.GetPropertyNameId(i);

            if (!propertyNames[i].Contains("unity"))
            {
                if (shad.GetPropertyType(i) == ShaderPropertyType.Texture)
                {
                    texParam.Add(new TextureParam());
                    texParam[texParam.Count - 1].paramName = propertyNames[i];
                    if (_rend is SpriteRenderer spriterend)
                    {
                        texParam[texParam.Count - 1].texture = spriterend.sprite.texture;
                    }
                    else
                    {
                        texParam[texParam.Count - 1].texture = (Texture2D)_rend.sharedMaterial.GetTexture(propId);
                    }
                }
                else if (shad.GetPropertyType(i) == ShaderPropertyType.Float || shad.GetPropertyType(i) == ShaderPropertyType.Range)
                {
                    floatParam.Add(new FloatParam());
                    floatParam[floatParam.Count - 1].paramName = propertyNames[i];
                    floatParam[floatParam.Count - 1].f = _rend.sharedMaterial.GetFloat(propId);
                }
                else if (shad.GetPropertyType(i) == ShaderPropertyType.Vector)
                {
                    vectorParam.Add(new VectorParam());
                    vectorParam[vectorParam.Count - 1].paramName = propertyNames[i];
                    vectorParam[vectorParam.Count - 1].vec = _rend.sharedMaterial.GetVector(propId);
                }
                else
                {
                    colorParam.Add(new ColorParam());
                    colorParam[colorParam.Count - 1].paramName = propertyNames[i];
                    colorParam[colorParam.Count - 1].col = _rend.sharedMaterial.GetColor(propId);
                }
            }
        }
    }
}

[Serializable]
class TextureParam
{
    [ReadOnly]
    public string paramName;
    public Texture2D texture;
}

[Serializable]
class IntParam
{
    [ReadOnly]
    public string paramName;
    public int i;
}

[Serializable]
class FloatParam
{
    [ReadOnly]
    public string paramName;
    public float f;
}

[Serializable]
class VectorParam
{
    [ReadOnly]
    public string paramName;
    public Vector4 vec;
}
[Serializable]
class ColorParam
{
    [ReadOnly]
    public string paramName;
    public Color col;
}