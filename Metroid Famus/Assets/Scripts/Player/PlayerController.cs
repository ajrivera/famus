using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerController : MonoBehaviour
{
    public static Vector3 PlayerPosition;
    
    private Rigidbody2D rb;
    private LayerMask floordetect;
    private RaycastHit2D hit;

    private bool jump;
    private bool right;
    private bool left;
    private bool dash;

    private float bottompoise;
    
    private bool canjump = true;
    private bool Jdelay;
    private bool candash = true;
    private bool dashing;
    private bool lookright;

    private bool changesideleft;
    private bool changesideright = true;
    
    private Camera cam;
    
    public GameObject ind;
    public Transform PlayerSprite;
    public Transform PlayerTracks;
    public float tracksSpd;
    private Renderer PlayerRend;
    public GameObject ClawArm;
    private bool clawCD = true;
    public GameObject RailArm;

    private MaterialPropertyBlock properties;
    
    
    private void Awake()
    {
        cam = Camera.main;
        rb =  GetComponent<Rigidbody2D>();
        floordetect = LayerMask.GetMask("Floor");
        bottompoise = -GetComponent<BoxCollider2D>().size.y/2;
        PlayerRend = PlayerTracks.GetComponent<Renderer>();
        properties = new MaterialPropertyBlock();
    }

    void Start()
    {
        
    }

    void Update()
    {
        PlayerPosition = transform.position;
        PlayerInput();
    }

    private void FixedUpdate()
    {
        hit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y + bottompoise), Vector2.down, 0.08f,
            floordetect);
        if (hit && !Jdelay)
            canjump = true;

        PlayerAction();
    }

    void OnGUI()
    {

        Vector3 point = new Vector3();
        Event currentEvent = Event.current;
        Vector2 mousePos = new Vector2();
        
        mousePos.x = currentEvent.mousePosition.x;
        mousePos.y = cam.pixelHeight - currentEvent.mousePosition.y;
        point = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, -cam.transform.position.z));
        // print(point);
        ind.transform.position = point;
        
        if (point.x < PlayerSprite.position.x && !changesideleft)
        {
            PlayerSprite.localScale = new Vector3(PlayerSprite.localScale.x*-1,PlayerSprite.localScale.y,PlayerSprite.localScale.z);
            changesideleft = true;
            changesideright = false;
        }else if (point.x > PlayerSprite.position.x && !changesideright)
        {
            PlayerSprite.localScale = new Vector3(PlayerSprite.localScale.x*-1,PlayerSprite.localScale.y,PlayerSprite.localScale.z);
            changesideright = true;
            changesideleft = false;
        }
    }

    private void OnDrawGizmos()
    {
        Debug.DrawRay(new Vector3(transform.position.x, transform.position.y + bottompoise,0),Vector3.down, Color.red);
    }

    void PlayerInput()
    {
        if (Input.GetKeyDown(KeyCode.W))
            jump = true;
        else if (Input.GetKeyUp(KeyCode.W))
            jump = false;
        if (Input.GetKey(KeyCode.A))
        {
            left = true;
            if (lookright)
                lookright = false;
        }else if (Input.GetKeyUp(KeyCode.A))
            left = false;

        if (Input.GetKey(KeyCode.D))
        {
            right = true;
            if (!lookright)
                lookright = true;
        }else if (Input.GetKeyUp(KeyCode.D))
            right = false;
        if (Input.GetKeyDown(KeyCode.S)){}

        if (Input.GetMouseButtonDown(0))
        {
            if (clawCD)
                StartCoroutine(ClawAttack());
        }
        
        if (candash && Input.GetKeyDown(KeyCode.LeftShift))
            dash = true;
        
        PlayerRend.GetPropertyBlock(properties);
        properties.SetFloat("_ScrollSpdX", -tracksSpd * Input.GetAxisRaw("Horizontal"));
        PlayerRend.SetPropertyBlock(properties);
        
    }

    void PlayerAction()
    {
        if(jump)
            Jump();
        if(right && !dashing)
        {
            rb.velocity = new Vector2(2, rb.velocity.y);
        }
        if(left && !dashing)
        {
            rb.velocity = new Vector2(-2, rb.velocity.y);
        }
        if(dash)
            Dash();

        if (!Input.anyKey)
        {
            if (dashing)
                rb.velocity = new Vector2(rb.velocity.x, 0);
            else
                rb.velocity = new Vector2(0, rb.velocity.y);
            //print("not moving or dashing");
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        
    }

    private void Dash()
    {
        dash = false;
        StartCoroutine(DashCD());
    }

    private IEnumerator DashCD()
    {
        candash = false;
        dashing = true;
        transform.GetComponent<HealthControlPlayer>().iframe = true;
        rb.AddForce(new Vector2((lookright ? 5f : -5f),0),ForceMode2D.Impulse);
        yield return new WaitForSeconds(0.3f);
        rb.velocity = new Vector2(0, rb.velocity.y);
        dashing = false;
        transform.GetComponent<HealthControlPlayer>().iframe = false;
        yield return new WaitForSeconds(0.7f);
        candash = true;
    }
    private void Jump()
    {
        if (!canjump) return;
        StartCoroutine(JumpDelay());
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.AddForce(new Vector2(rb.velocity.x,300));
        canjump = false;
        // StartCoroutine(JumpCD());

    }
    
    private IEnumerator ClawAttack()
    {
            clawCD = false;
            ClawArm.GetComponent<CircleCollider2D>().enabled = true;
            print("clawON");
            yield return new WaitForSeconds(0.4f);
            ClawArm.GetComponent<CircleCollider2D>().enabled = false;
            print("clawOFF");
            clawCD = true;
    }
    
    private IEnumerator JumpDelay()
    {
        Jdelay = true;
        yield return new WaitForSeconds(0.2f);
        Jdelay = false;
    }
    
    
    // private IEnumerator JumpCD()
    // {
    //     canjump = false;
    //     yield return new WaitForSeconds(1f);
    //     print("true");
    //     canjump = true;
    // }
    
}
