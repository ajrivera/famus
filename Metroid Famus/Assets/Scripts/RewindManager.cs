using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewindManager : MonoBehaviour
{
    [SerializeField] private bool rewindInit;
    [SerializeField] private float rewindTimeInit;
    
    public static bool rewind;
    public static float rewindTime;
    void OnValidate()
    {
        rewind = rewindInit;
        rewindTime = rewindTimeInit;
    }
    
    void Update()
    {
        
    }
}
