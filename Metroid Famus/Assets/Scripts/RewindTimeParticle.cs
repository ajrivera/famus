using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

[ExecuteAlways]
public class RewindTimeParticle : MonoBehaviour
{
    public bool play;
    private float parSysDuration;
    private bool parSysLoop;
    private ParticleSystem parSys;
    private float time;
    
    void Start()
    {
        parSys = GetComponent<ParticleSystem>();
        parSysDuration = parSys.main.duration;
        parSysLoop = parSys.main.loop;
    }
    

    void Update()
    {
        switch (play)
        {
            case true when !RewindManager.rewind:
            {
                if (time < parSysDuration + RewindManager.rewindTime)
                {
                    time += Time.deltaTime;
                }
                else if (parSysLoop)
                {
                    time += Time.deltaTime;
                }
                else
                {
                    play = false;
                    time = 0;
                }

                break;
            }
            case true when RewindManager.rewind:
            {
                if (time >= 0)
                {
                    time -= Time.deltaTime;
                }
                else
                {
                    play = false;
                    time = 0;
                }

                break;
            }
        }

        parSys.Simulate(time, true, time > 0);
    }
}
