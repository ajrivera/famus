using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Arms : so_EquipableObjects
{
    public bool isEnabled;
    public int cost; //Any type of weight or somethin?
    public enum TypeArm
    {
        CLAW,
        RAIL
    }

    public TypeArm type;
}
