using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class so_EquipableObjects : ScriptableObject
{
    public string nameObject;
    public string description;
    public int atk;
    public bool equiped;
    public GameObject sprite;
}
